import keystone

# This generates some very simple ARM machine code for us, 
# from a trivial (snippet) of ARM ASM code.

ks = keystone.Ks(keystone.KS_ARCH_ARM, keystone.KS_MODE_ARM)

raw = (ks.asm("mov r0, #0x15 \n mov r1, #0x22 \n add r2, r1, r0"))

# The return from ks.asm is a tuple, with the machine-code as a list 
# of bytes in the first element, and the number of instructions that
# were assembled in the second.

# We just want the bytes, so we'll print (as bytes) the 0th element.
print(bytes(raw[0]))
