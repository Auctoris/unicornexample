import unicorn
from unicorn.arm_const import *
import logging
import BracketFormatter

# We'll start with our machine code bytes, generated
# by the code generator in the other program...

MCODE = b'\x15\x00\xa0\xe3"\x10\xa0\xe3\x00 \x81\xe0'

# We'll use logging output - because we can...
# So we'll set that up first.
logger = logging.getLogger()
logger.setLevel(logging.INFO)
print_logger = logging.StreamHandler()
print_logger.setLevel(logging.INFO)
print_logger.setFormatter(BracketFormatter.BracketFormatter())
logger.addHandler(print_logger)

# Now setup a unicorn object for ARM mode...
uc = unicorn.Uc(unicorn.UC_ARCH_ARM, unicorn.UC_MODE_ARM)

# We'll define some emulated memory - starting at
# 0x100000, and with a size of 2Mb

START_ADDRESS = 0x10000
uc.mem_map(START_ADDRESS, 2 * 1024 * 1024)

# We'll now load the machine code into that 
# memory... Since this is a bare-metal example
# we don't need to do anything more than just copy
# the bytes...

uc.mem_write(START_ADDRESS, MCODE)

# We will initialize the register values too...
uc.reg_write(UC_ARM_REG_R0, 0x00)
uc.reg_write(UC_ARM_REG_R1, 0x00)
uc.reg_write(UC_ARM_REG_R2, 0x00)

# We don't need to explicitly set the PC... as this
# will be taken care of when we start the emulation.

logger.info(f"Starting emulation at 0x{START_ADDRESS:x}")

# We specify the start & end addresses...
uc.emu_start(START_ADDRESS, START_ADDRESS + len(MCODE))
logger.info(f"Emulation complete.")

# Now we read the emulated registers...
r0 = uc.reg_read(UC_ARM_REG_R0)
r1 = uc.reg_read(UC_ARM_REG_R1)
r2 = uc.reg_read(UC_ARM_REG_R2)

# And finally print them.
logger.info(f"Register r0: {r0}")
logger.info(f"Register r1: {r1}")
logger.info(f"Register r2: {r2}")
